import React, { useCallback, useState } from "react";
import { 
  Box,
  Flex
} from "@chakra-ui/react";
import FilterInput from "../components/FilterInput";
import ProfileList from "../components/ProfileList";

function Profiles() {
  const [filterKeyword, setFilterKeyword] = useState<string>("");

  const changeFilterKeyword = useCallback((e) => {
    setFilterKeyword(e.target.value);
  }, []);

  return (
    <>
      <Flex justifyContent="center">
        <FilterInput onChange={changeFilterKeyword} />
      </Flex>
      <Box mt="10px">
        <ProfileList filterKeyword={filterKeyword} />
      </Box>
    </>
  );
}

export default Profiles;