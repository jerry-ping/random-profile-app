import React from "react";
import {
  Input
} from "@chakra-ui/react";

type FilterInputProps = {
  onChange: (e: any) => void;
}

const FilterInput = ({ onChange }: FilterInputProps) => {
  return (
    <Input onChange={onChange} w="300px" maxW="full" color="white" fontSize="18" placeholder='Filter names' />
  );
}

export default React.memo(FilterInput);