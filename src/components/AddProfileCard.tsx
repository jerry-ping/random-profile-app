import React from "react";
import { 
  Flex,
  Text
} from "@chakra-ui/react";
import { AddIcon } from '@chakra-ui/icons'

type AddProfileCardProps = {
  onClick: () => void;
}

const AddProfileCard = ({ onClick }: AddProfileCardProps) => {
  return (
    <Flex flexDir="column" justifyContent="center" alignItems="center" onClick={onClick} w="full" h="full" p="20px" color="white" bgGradient="linear(to-r, #DE2673, #FE4F31)" cursor="pointer">
      <AddIcon w={10} h={10} />
      <Text mt="10px" fontSize="md">Add Profile</Text>
      <Text mt="10px" fontSize="xs">https://randomuser.me/api/</Text>
    </Flex>
  );
}

export default React.memo(AddProfileCard);