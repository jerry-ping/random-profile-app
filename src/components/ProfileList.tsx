import React, { useCallback, useState } from "react";
import { 
  Flex,
  Box
} from "@chakra-ui/react";
import SingleProfileCard from "./SingleProfileCard";
import AddProfileCard from "./AddProfileCard";
import * as api from "../api/api";

type ProfileListProps = {
  filterKeyword: string
}

const ProfileList = ({ filterKeyword }: ProfileListProps) => {
  const [profileData, setProfileData] = useState<any[]>([]);

  const addProfile = useCallback(async () => {
    const result: any[] = await api.fetchProfileData();

    setProfileData(profileData => [...profileData, ...result]);
  }, []);

  return (
    <Flex wrap="wrap">
      {profileData
        .filter(data => `${data.name.first.toLowerCase()} ${data.name.last.toLowerCase()}`.includes(filterKeyword.toLowerCase()))
        .map((data, index) => (
          <Box className="card" key={index}>
            <SingleProfileCard name={data.name} email={data.email} phone={data.phone} picture={data.picture.medium} />
          </Box>
      ))}
      <Box className="card">
        <AddProfileCard onClick={addProfile} />
      </Box>
    </Flex>
  );
}

export default React.memo(ProfileList);