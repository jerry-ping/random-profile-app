import React from "react";
import {
  Flex,
  Box,
  Image,
  Text
} from "@chakra-ui/react";

type Name = {
  title: string;
  first: string;
  last: string;
};

type SingleProfileCardProps = {
  name: Name,
  email: string,
  phone: string,
  picture: string,
}

const SingleProfileCard = ({ name, email, phone, picture }: SingleProfileCardProps) => {
  return (
    <Box w="full" h="full" color="white" p="20px" textAlign="left" bgGradient="linear(to-r, #11232B, #2B4E63)">
      <Flex justifyContent="center">
        <Image borderRadius="50%" border="2px solid white" w="70%" src={picture} alt="Profile Image" />
      </Flex>
      <Text fontSize='md' mt="5px">{name['first']} {name['last']}</Text>
      <Text fontSize='10px' mt="5px">Email</Text>
      <Text fontSize='xs' mt="3px">{email}</Text>
      <Text fontSize='10px' mt="5px">Phone</Text>
      <Text fontSize='xs' mt="3px">{phone}</Text>
    </Box>
  );
}

export default React.memo(SingleProfileCard);