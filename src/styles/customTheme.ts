import { extendTheme } from "@chakra-ui/react";

const customTheme = extendTheme({
  styles: {
    global: (props: any) => ({
      boxSizing: "border-box",
      ".card": {
        width: "200px",
        height: "280px",
        margin: "10px",
        boxShadow: "5px 5px 10px black",
        borderRadius: "10px",
        overflow: "hidden",
        [`@media screen and (max-width: ${props.theme.breakpoints.sm})`]: {
          width: "100%",
          height: "auto"
        }
      }
    }),
  },
});

export default customTheme;
