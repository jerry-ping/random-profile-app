async function fetchProfileData() {
  try {
    const respone = await fetch('https://randomuser.me/api/');
    const data = await respone.json();
  
    return data.results;
  } catch (e) {
    console.error(e);
  }
}

export {
  fetchProfileData,
}