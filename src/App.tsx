import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { 
  ChakraProvider,
  Box,
  Flex
} from "@chakra-ui/react";
import "./App.css";
import Profiles from "./pages/Profiles";
import customTheme from "./styles/customTheme";

function App() {
  return (
    <div className="App">
      <ChakraProvider theme={customTheme}>
        <Flex w="full" minH="100vh" bgGradient="linear(to-r, #162135, #223650)" justifyContent="center">
          <Box w="full" maxW={{ "base": "100%", "md": "1400px" }} p={{ base: "20px", md: "40px" }}>
            <Router>
              <Switch>
                <Route exact path="/">
                  <Profiles />
                </Route>
                <Route exact path="*">
                  <Redirect to="/" />
                </Route>
              </Switch>
            </Router>
          </Box>
        </Flex>
      </ChakraProvider>
    </div>
  );
}

export default App;
